(ns apix.test.util
	(:use [apix.util]
		  [clojure.test])
	(:import [java.util Calendar]))

(defn soma [a b] (if (< a 10) false (+ a b)))

(deftest test-retrying
	(is (= false (retrying #(soma 1 4) 3)) "soma fn with 1 and 4 sould be false")
	(is (= 16 (retrying #(soma 12 4) 3)) "soma fn with 12 and 4 should be 16"))

(System/setProperty "SDB_TICKETS_DOMAIN" "tickets-dev")

(deftest test-dcurdom
	(let [c (Calendar/getInstance *tz*)
		  f #(.get c %)
		  year (f Calendar/YEAR)
		  month (f Calendar/MONTH)]
	(is (= (str "tickets-dev-" year month) (dcurdom)) "Current domain should be right")))