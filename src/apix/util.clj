(ns apix.util
    (:require [clojure.string :as str])
    (import [java.net URI]
            [java.util Calendar]))

(defn env
	"Just wraps the ENV_VAR get from system environment."
	([k]
        (env k ""))
    ([k opt] 
	   (get (System/getenv) k (or opt ""))))

(defmacro every
    "wraps a serie of functions f to be called every t seconds."
    [t & f]
    `(let [t# (* 1000 ~t)]
         (loop []
             (do ~@f)
             (. Thread (sleep t#)))
             (recur [])))
;; Wraps format
(def $ format)

(defn kvstring 
    "Every key and value of a map becomes strings. Warn: just for flat maps."
    [m]
    (loop [i m o {}]
        (let [[k v] (first i)]
            (if (= 0 (count i))
                o
            (recur (rest i) (merge o [(name k) (str v)]))))))

(defn predis-uri [u]
    (let [uri (URI. u)
          uinfo (.getUserInfo uri)]
          {:host (.getHost uri)
           :port (.getPort uri)
           :password (and uinfo (last (str/split uinfo #":")))}))


(defn ltm
     "Makes a clojure map from a given collection. lst should have an even number of elements"
	  [lst]
	  {:pre [(even? (count lst))]}
	  (reduce merge
	      (map (fn [l] {(keyword (str (first l))) (second l)}) (partition 2 lst))))

(defn unkeyword [m]
	(reduce merge {} 
	   (map #(hash-map (name (first %)) (second %)) m)))

(defonce *tz* (java.util.TimeZone/getTimeZone "UTC"))

(defn 
    curdom 
    "Returns the name of the current domain in SDB. It is based on 
    date. The current implementation returns yearmonth. More sofisticated
    sdb domain rotation are comming."
    {:static true}
    ([prefix tz]
    (let [c (Calendar/getInstance tz)
          f #(.get c %)
          year (f Calendar/YEAR)
          month (f Calendar/MONTH)]
        (str prefix "-" year month))))

(defn dcurdom [] (curdom (env "SDB_TICKETS_DOMAIN" "tickets-dev") *tz*))

(defn retrying 
    "f is a boolean function with no args. f will be invoked at most n times
    with no interval. A f failure is represented by false. If it succeed 
    the returned value is passed to the caller.
    n equals 0 is same as n equals 1.

    Example:
        (defn soma [a b] (if (< a 10) false (+ a b)))
        (retrying #(soma 1 4) 3) 
        => false
        (retrying #(soma 12 4) 3)
        => 16"
    [f n]   
        (or (f) 
            (and (> n 1)
            (retrying f (dec n)))))

