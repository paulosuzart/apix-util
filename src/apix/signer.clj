(ns apix.signer
	(:import [org.apache.commons.codec.binary Base64] 
			 [java.security SignatureException]
	  		 [javax.crypto Mac]
	  		 [javax.crypto.spec SecretKeySpec]
			 [java.net URLEncoder]))

;; Signature alg
(defonce alg "HmacSHA1")

(defn sign
	"Content should be URLEncoded."

	[^String sign-key ^String content]
	(let [skey (new SecretKeySpec (.getBytes sign-key) alg)
		  mac  (doto (Mac/getInstance alg) (.init skey))
		  raw  (.doFinal mac (.getBytes content))]
		(let [s (new String (Base64/encodeBase64 raw))]
			(println "###" s)
			s)))


(defn prepare-params 
	"Given a param-map with all request parameter. Sorts it and
	creates a string to be used by signature functions. 
	TODO: let the encode optional"
	[param-map]
	(URLEncoder/encode 
		(reduce 
			str 
			(map #(str (first %) (second %)) 
				(sort param-map)))))

(defn sign-params
 	"Removes parameter values with 'Signature' key"
 	[^String sign-key param-map]
		(let [content (prepare-params (dissoc param-map "Signature"))]
			(sign sign-key content)))




